// author caorong
// create at 2014-08-24 15:36

#include <stdio.h>
#include <ctype.h>

int main(int argc, const char *argv[]) {
    printf("%d\n", isalpha('1'));   
    printf("%d\n", isalpha('a'));   
    printf("%d\n", isalpha('!'));   
    
    printf("============\n");
    printf("%d\n", isblank('!'));   
    printf("%d\n", isblank(' '));   


    return 0;
}
