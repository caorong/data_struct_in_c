// author caorong
// create at 2014-08-24 15:44

#include <stdio.h>

int main(int argc, const char *argv[]) {
    // create two arrays we care about
    int ages[] = {23, 43, 12, 89, 2};
    char *names[] = {
        "Alan", "Frank",
        "Mary", "John", "Lisa"
    };

    // safely get the size of ages
    int count = sizeof(ages) / sizeof(int);
    int i = 0;

    // first way using indexing
    for(i = 0; i < count; i++) {
        printf("%s has %d years alive.\n",
                names[i], ages[i]);
    }

    printf("---\n");

    // setup the pointers to the start of the arrays
    int *cur_age = ages;
    char **cur_name = names;
    /* char *cur_name[] = names; */

    // second way using pointers
    for(i = 0; i < count; i++) {
        printf("%s is %d years old.\n",
                *(cur_name+i), *(cur_age+i));
    }

    printf("---\n");

    // third way, pointers are just arrays
    for(i = 0; i < count; i++) {
        printf("%s is %d years old again.\n",
                cur_name[i], cur_age[i]);
    }

    printf("---\n");

    // fourth way with pointers in a stupid complex way
    for(cur_name = names, cur_age = ages;
            (cur_age - ages) < count;
            cur_name++, cur_age++)
    {
        printf("%s lived %d years so far.\n",
                *cur_name, *cur_age);
    }
    
    printf("%p \n", &ages);
    /* printf("%p \n", &ages); */
    /* printf("%p \n", &ages[0]); */
    /* printf("%p \n", &ages[1]); */

    printf("%p \n", names);
    printf("%p \n", names+1);
    printf("%p \n", cur_name);
    printf("%p \n", cur_name+1);
    printf("%p \n", &(*cur_name));


    return 0;
}
