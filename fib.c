// author caorong
// create at 2014-08-23 18:00

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

int cache[100] = {-1};

int fib1(int x){
    if (x <= 1){
        return 1;
    } else {
        int f1 = 0,f2 = 0;
        if(cache[x-1] != -1)
            f1 = cache[x-1];
        else{
            f1 = fib1(x-1);
            cache[x-1] = f1;
        }
        if(cache[x-2] != -1)
            f2 = cache[x-2];
        else {
            f2 = fib1(x-2);
            cache[x-2] = f2;
        }
        return f1 + f2;
    }
}

int fib(int x){
    if (x <= 1){
        return 1;
    } else {
        return fib(x-1) + fib(x-2);
    }
}

unsigned long now(){
    struct timeval tv;
    gettimeofday(&tv,NULL);
    unsigned long time_in_micros = 1000000 * tv.tv_sec + tv.tv_usec;
    return time_in_micros;
}

int main(int argc, const char *argv[]) {
    //printf("%d\n",atoi(argv[1]));
    for(int i=0;i<100;i++){
        cache[i] = -1;
    }
    /* printf("%d\n", cache[1]); */
    unsigned long start,end;
    /* start = time(NULL); */
    start = now();
    int re = fib(atoi(argv[1]));
    end = now();
    printf("fib: %d, costs %ld, without cache\n", re, end - start);
    
    start = now();
    re = fib1(atoi(argv[1]));
    end = now();
    printf("fib: %d, costs %ld, with cache\n", re, end - start);
    /* fprintf(stdout, "%ld\n", (long)time(NULL));  */
    /* printf("%ld\n", (long)time(NULL));  */
    /* struct timeval tv; */
    /* gettimeofday(&tv,NULL); */
    /* unsigned long time_in_micros = 1000000 * tv.tv_sec + tv.tv_usec; */
    /* printf("%ld\n", time_in_micros); */
    return 0;
}

