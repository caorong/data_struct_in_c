// 2014-07-02 22:11
#include <stdio.h>
#include <stdlib.h>

#include "List.h"

List MakeEmpty(List l){
    /* struct Node *l = (struct Node*)malloc(sizeof(struct Node)); */
    l = (struct Node*)malloc(sizeof(struct Node));
    l->Next = NULL;
    l->Element = NULL;
    /* head = (struct Node *)malloc(sizeof(struct Node)); */
    /* head.Element = NULL; */
    /* head.Next = NULL; */
    /* return head; */
    return l;
}

int IsEmpty(List L){
    return L->Next == NULL;
}

int IsLast(Position P, List L){
    return P->Next == NULL;
}

Position Find(ElementType X, List L){
    Position p;
    
    p = L->Next;
    while(p!=NULL && p->Element != X){
        p = p->Next;
    }
    return p;
}

void Delete(ElementType X, List L){
    Position P, TmpCell;
    
    P = FindPrevious(X, L);
    if(!IsLast(X,L)){
        TmpCell = P->Next;
        P->Next = TmpCell->Next; 
        free(TmpCell);
    }
}

Position FindPrevious(ElementType X, List L){

}


int main(int argc, char** argv) {
    List l;
    l = MakeEmpty(l);
    printf("%d\n",l->Element);
}
